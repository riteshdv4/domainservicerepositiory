package com.example.interactor;

import com.example.executer.PostExecutionThread;
import com.example.executer.ThreadExecutor;
import com.example.model.MainDominModel;
import com.example.repository.DataRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by lp-ritesh on 3/6/17.
 */

public class MainActivityUseCase extends UseCase<MainDominModel, MainActivityUseCase.Params> {

    private DataRepository mDataRepository;

    @Inject
    MainActivityUseCase(DataRepository dataRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.mDataRepository = dataRepository;
    }

    @Override
    Observable<MainDominModel> buildUseCaseObservable(Params params) {
        return mDataRepository.getMainActivityData(params.url);
    }

    public static class Params {
        String url;

        public Params(String url) {
            this.url = url;
        }

        public static Params getMainActivityDetails(String url) {
            return new Params(url);
        }


    }
}
