package com.example.repository;

import com.example.model.MainDominModel;

import io.reactivex.Observable;

/**
 * Created by lp-ritesh on 3/6/17.
 */

public interface DataRepository {

    Observable<MainDominModel> getMainActivityData(String url);

}
