package com.example.model;

/**
 * Created by lp-ritesh on 3/6/17.
 */

public class MainDominModel {
    String id;
    String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }
}
