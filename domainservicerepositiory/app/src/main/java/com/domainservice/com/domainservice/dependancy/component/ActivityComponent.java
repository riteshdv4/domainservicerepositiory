package com.domainservice.com.domainservice.dependancy.component;

import android.app.Activity;

import com.domainservice.com.domainservice.dependancy.PerActivity;
import com.domainservice.com.domainservice.dependancy.module.ActivityModule;

import dagger.Component;

/**
 * Created by lp-ritesh on 3/6/17.
 */

@SuppressWarnings({"WeakerAccess", "unused"})
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    Activity activity();
}
