package com.domainservice.com.domainservice.dependancy.module;

import com.domainservice.com.domainservice.interfaces.IMainActionListener;
import com.domainservice.com.domainservice.presenter.MainActivityPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lp-ritesh on 3/6/17.
 */
@Module
public class MainModule {

    @Provides
    IMainActionListener mainActionListener(MainActivityPresenter presenter) {
        return presenter;
    }
}
