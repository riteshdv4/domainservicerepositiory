package com.domainservice.com.domainservice.dependancy.module;

import android.app.Activity;

import com.domainservice.com.domainservice.dependancy.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lp-ritesh on 3/6/17.
 */

@Module
public class ActivityModule {


    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    /**
     * Expose Activity to dependent in graph
     */

    @Provides
    @PerActivity
    Activity activity() {
        return this.activity;
    }


}
