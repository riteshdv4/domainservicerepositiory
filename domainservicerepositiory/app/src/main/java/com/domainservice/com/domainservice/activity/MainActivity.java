package com.domainservice.com.domainservice.activity;

import android.os.Bundle;

import com.domainservice.com.domainservice.R;
import com.domainservice.com.domainservice.dependancy.HasComponent;
import com.domainservice.com.domainservice.dependancy.component.DaggerMainComponent;
import com.domainservice.com.domainservice.dependancy.component.MainComponent;
import com.domainservice.com.domainservice.interfaces.IMainActionListener;
import com.domainservice.com.domainservice.view.MainView;

import javax.inject.Inject;


public class MainActivity extends BaseActivity implements MainView, HasComponent<MainComponent> {

    private MainComponent mainComponent;

    @Inject
    IMainActionListener mainActionListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeInjector();
        this.getComponent().inject(this);

        mainActionListener.setView(this);
        mainActionListener.callApi();

    }

    private void initializeInjector() {
        this.mainComponent = DaggerMainComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build();

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public MainComponent getComponent() {
        return mainComponent;
    }

}
