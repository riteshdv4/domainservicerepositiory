package com.domainservice.com.domainservice.dependancy.module;

import android.content.Context;

import com.domainservice.com.domainservice.MyApplication;
import com.domainservice.com.domainservice.view.UIThread;
import com.domainservice.com.service.executor.JobExecutor;
import com.domainservice.com.service.repository.BFFDataRepository;
import com.example.executer.PostExecutionThread;
import com.example.executer.ThreadExecutor;
import com.example.repository.DataRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lp-ritesh on 3/6/17.
 */

@Module
public class ApplicationModule {


    private final MyApplication application;

    public ApplicationModule(MyApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.application;
    }

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }


    @Provides
    @Singleton
    DataRepository provideUserRepository(BFFDataRepository userDataRepository) {
        return userDataRepository;
    }

}
