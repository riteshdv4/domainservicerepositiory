package com.domainservice.com.domainservice.view;

/**
 * Created by lp-ritesh on 3/6/17.
 */

public interface BaseView {
    void showLoading();

    void hideLoading();

    void showError(String message);

}
