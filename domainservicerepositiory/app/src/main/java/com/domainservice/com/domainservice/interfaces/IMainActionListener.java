package com.domainservice.com.domainservice.interfaces;

/**
 * Created by lp-ritesh on 3/6/17.
 */

public interface IMainActionListener<T> {

    void setView(T t);

    void callApi();

}
