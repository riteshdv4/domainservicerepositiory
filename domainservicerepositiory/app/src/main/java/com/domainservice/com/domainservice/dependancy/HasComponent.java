package com.domainservice.com.domainservice.dependancy;

/**
 * Created by lp-ritesh on 3/6/17.
 */

public interface HasComponent<C> {
    C getComponent();
}
