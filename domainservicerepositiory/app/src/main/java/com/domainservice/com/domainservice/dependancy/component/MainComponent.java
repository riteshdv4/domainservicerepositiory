package com.domainservice.com.domainservice.dependancy.component;

import com.domainservice.com.domainservice.activity.MainActivity;
import com.domainservice.com.domainservice.dependancy.PerActivity;
import com.domainservice.com.domainservice.dependancy.module.ActivityModule;
import com.domainservice.com.domainservice.dependancy.module.MainModule;

import dagger.Component;

/**
 * Created by lp-ritesh on 3/6/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, MainModule.class})
public interface MainComponent extends ActivityComponent {

    void inject(MainActivity mainActivity);
}
