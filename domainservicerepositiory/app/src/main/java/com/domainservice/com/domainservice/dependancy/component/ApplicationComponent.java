package com.domainservice.com.domainservice.dependancy.component;

import android.content.Context;

import com.domainservice.com.domainservice.activity.BaseActivity;
import com.domainservice.com.domainservice.dependancy.module.ApplicationModule;
import com.example.executer.PostExecutionThread;
import com.example.executer.ThreadExecutor;
import com.example.repository.DataRepository;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by lp-ritesh on 3/6/17.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(BaseActivity baseActivity);

    Context context();

    ThreadExecutor threadExecutor();

    PostExecutionThread postExecutionThread();

    DataRepository dataRepository();
}
