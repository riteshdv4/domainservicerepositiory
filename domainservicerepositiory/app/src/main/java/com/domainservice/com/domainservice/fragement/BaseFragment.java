package com.domainservice.com.domainservice.fragement;

import android.support.v4.app.Fragment;

import com.domainservice.com.domainservice.dependancy.HasComponent;

/**
 * Created by lp-ritesh on 3/6/17.
 */

public abstract class BaseFragment extends Fragment {

    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>)getActivity()).getComponent());

    }

}
