package com.domainservice.com.domainservice.presenter;

import android.content.Context;

import com.domainservice.com.domainservice.dependancy.PerActivity;
import com.domainservice.com.domainservice.interfaces.IMainActionListener;
import com.domainservice.com.domainservice.view.MainView;
import com.example.interactor.DefaultObserver;
import com.example.interactor.MainActivityUseCase;
import com.example.model.MainDominModel;

import javax.inject.Inject;

/**
 * Created by lp-ritesh on 3/6/17.
 */
@PerActivity
public class MainActivityPresenter extends Presenter implements IMainActionListener<MainView> {

    private Context mContext;

    private MainView mMainView;

    private MainActivityUseCase mMainActivityUseCase;

    @Inject
    public MainActivityPresenter(Context context, MainActivityUseCase mainActivityUseCase) {
        this.mContext = context;
        this.mMainActivityUseCase = mainActivityUseCase;
    }

    @Override
    public void setView(MainView mainView) {
        this.mMainView = mainView;

    }

    @Override
    public void callApi() {
        String url = "";
        MainActivityUseCase.Params mainParams = MainActivityUseCase.Params.getMainActivityDetails(url);
        mMainActivityUseCase.execute(new MainActivityObserver(), mainParams);
    }


    private class MainActivityObserver extends DefaultObserver<MainDominModel> {
        @Override
        public void onNext(MainDominModel mainModel) {
            super.onNext(mainModel);
        }

        @Override
        public void onError(Throwable exception) {
            super.onError(exception);
            mMainView.hideLoading();
        }

        @Override
        public void onComplete() {
            super.onComplete();
            mMainView.hideLoading();

        }
    }
}
