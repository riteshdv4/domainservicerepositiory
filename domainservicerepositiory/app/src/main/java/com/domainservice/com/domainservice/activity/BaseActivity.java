package com.domainservice.com.domainservice.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.domainservice.com.domainservice.MyApplication;
import com.domainservice.com.domainservice.dependancy.component.ApplicationComponent;
import com.domainservice.com.domainservice.dependancy.module.ActivityModule;

/**
 * Created by lp-ritesh on 3/6/17.
 */

public class BaseActivity extends Activity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getApplicationComponent().inject(this);
    }


    /**
     * Get Main Application Component for dependancy injection
     *
     * @return {@link ApplicationComponent}
     */

    ApplicationComponent getApplicationComponent() {
        return ((MyApplication) getApplication()).getApplicationComponent();
    }


    /**
     * Get an Activity module for Dependancy injection
     *
     * @return {@link ActivityModule}
     */
    ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }


}
