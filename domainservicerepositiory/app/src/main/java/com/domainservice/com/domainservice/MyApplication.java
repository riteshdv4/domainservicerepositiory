package com.domainservice.com.domainservice;

import android.app.Application;

import com.domainservice.com.domainservice.dependancy.component.ApplicationComponent;
import com.domainservice.com.domainservice.dependancy.component.DaggerApplicationComponent;
import com.domainservice.com.domainservice.dependancy.module.ApplicationModule;

/**
 * Created by lp-ritesh on 3/6/17.
 */

public class MyApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeInjector();

    }

    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }

}
