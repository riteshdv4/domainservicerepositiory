package com.domainservice.com.service.repository.datastore;

import android.content.Context;
import android.support.annotation.NonNull;

import com.domainservice.com.service.entity.mapper.EntityJsonMapper;
import com.domainservice.com.service.net.RestApi;
import com.domainservice.com.service.net.RestApiImpl;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Factory that creates different implementations of {@link BFFDataSource}
 * Created by sushil on 7/4/17.
 */
@Singleton
public class DataStoreFactory {

    private final Context context;

    @Inject
    DataStoreFactory(@NonNull Context context) {
        this.context = context.getApplicationContext();
    }

    public BFFDataSource createCloudDataSource() {

        final EntityJsonMapper entityJsonMapper = new EntityJsonMapper();

        final RestApi restApi = new RestApiImpl(this.context, entityJsonMapper);

        return new CloudUserDataSource(restApi);
    }
}
