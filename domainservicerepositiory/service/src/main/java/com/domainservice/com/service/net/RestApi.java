package com.domainservice.com.service.net;


import com.domainservice.com.service.entity.MainEntity;

import io.reactivex.Observable;

/**
 * RestApi for retrieving data from the network.
 */
public interface RestApi {


    String API_BASE_URL = "base_url";


    String HEADER_AUTHORIZATION = "Authorization";


    /**
     * Api url for getting configuration settings.
     */
    String API_URL_GET_MAIN_DATA = API_BASE_URL + "/mainData";


    /**
     * Retrieves an {@link Observable} which will emit a {@link com.domainservice.com.service.entity.MainEntity}.
     */
    Observable<MainEntity> getMainEntityObservable();


//    Observable<GangwayEntity> getGangwayDashboard(String url);
//
//    Observable<CheckInLocationDetailEntity> getCheckInLocationDetailEntity(String url);
//
//    Observable<ValidateDetailEntity> getValidateDetailsEntity(String url);

}
