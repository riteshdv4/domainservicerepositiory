package com.domainservice.com.service.entity.mapper;

import com.example.model.MainDominModel;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Mapper class used to transform entities (in the data layer) to Entities in the
 * domain layer.
 */
@Singleton
public class EntityDataMapper {

    @Inject
    EntityDataMapper() {
    }

    /**
     * Transform a {@link com.domainservice.com.service.entity.MainEntity} into an
     * {@link com.domainservice.com.service.entity.MainEntity}.
     *
     * @param mainEntity Object to be transformed.
     * @return {@link com.domainservice.com.service.entity.MainEntity} if valid
     * {@link com.domainservice.com.service.entity.MainEntity} otherwise null.
     */
    public MainDominModel transformmainEntity(com.domainservice.com.service.entity.MainEntity
                                                      mainEntity) {
        MainDominModel mainModel = null;
        if (mainEntity != null) {
            mainModel = new MainDominModel();

            mainModel.setId(mainEntity.getId());
            mainModel.setPassword(mainEntity.getPassword());
        }
        return mainModel;
    }


}
