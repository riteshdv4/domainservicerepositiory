package com.domainservice.com.service.repository.datastore;


import com.domainservice.com.service.entity.MainEntity;
import com.domainservice.com.service.net.RestApi;

import io.reactivex.Observable;

/**
 * {@link BFFDataSource} implementation based on connections to the api (Cloud).
 * <p>
 * Created by sushil on 7/4/17.
 */
public class CloudUserDataSource implements BFFDataSource {

    private final RestApi restApi;

    public CloudUserDataSource(RestApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public Observable<MainEntity> getMainEntityObservable() {
        return restApi.getMainEntityObservable();
    }
}
