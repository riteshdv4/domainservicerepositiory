package com.domainservice.com.service.repository.datastore;


import com.domainservice.com.service.entity.MainEntity;

import io.reactivex.Observable;

/**
 * Created by sushil on 7/4/17.
 */

public interface BFFDataSource {

    /**
     * Get an {@link Observable} which will emit a List of
     * {@link MainEntity}.
     */
    Observable<MainEntity> getMainEntityObservable();


}
