package com.domainservice.com.service.repository;


import com.domainservice.com.service.entity.mapper.EntityDataMapper;
import com.domainservice.com.service.repository.datastore.BFFDataSource;
import com.domainservice.com.service.repository.datastore.DataStoreFactory;
import com.example.model.MainDominModel;
import com.example.repository.DataRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * {@link DataRepository} for retrieving data.
 * Created by sushil on 7/4/17.
 */
@Singleton
public class BFFDataRepository implements DataRepository {

    private final DataStoreFactory dataStoreFactory;
    private final EntityDataMapper entityDataMapper;

    /**
     * Constructs a {@link DataRepository}.
     *
     * @param dataStoreFactory A factory to construct different data source implementations.
     */
    @Inject
    BFFDataRepository(DataStoreFactory dataStoreFactory, EntityDataMapper entityDataMapper) {
        this.dataStoreFactory = dataStoreFactory;
        this.entityDataMapper = entityDataMapper;
    }

    @Override
    public Observable<MainDominModel> getMainActivityData(String url) {
        final BFFDataSource dataSource = dataStoreFactory.createCloudDataSource();
        return dataSource.getMainEntityObservable().map(this.entityDataMapper::transformmainEntity);
    }
}
