package com.domainservice.com.service.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lp-ritesh on 3/6/17.
 */

public class MainEntity {


    @SerializedName("id")
    String id;
    @SerializedName("password")
    String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }
}
