package com.domainservice.com.service.entity.mapper;

import com.domainservice.com.service.entity.MainEntity;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import javax.inject.Inject;

/**
 * Class used to transform from Strings representing json to valid objects.
 */
public class EntityJsonMapper {

    private final Gson gson;

    @Inject
    public EntityJsonMapper() {
        this.gson = new Gson();
    }

    /**
     * Transform from valid json string to {@link }.
     *
     * @param jsonResponse A json representing a user profile.
     * @return {@link }.
     * @throws JsonSyntaxException if the json string is not a valid json structure.
     */
    public MainEntity transformMainEntity(String jsonResponse) throws JsonSyntaxException {
        final Type settingsEntityType = new TypeToken<MainEntity>() {
        }.getType();
        return this.gson.fromJson(jsonResponse, settingsEntityType);
    }
}
