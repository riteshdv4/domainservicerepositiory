package com.domainservice.com.service.net;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.domainservice.com.service.entity.MainEntity;
import com.domainservice.com.service.entity.mapper.EntityJsonMapper;
import com.google.gson.Gson;

import java.net.MalformedURLException;
import java.util.Map;

import io.reactivex.Observable;

/**
 * {@link RestApi} implementation for retrieving data from the network.
 */
public class RestApiImpl implements RestApi {

    private final Context context;
    private final Gson gson;
    private EntityJsonMapper entityJsonMapper;
    private CommonHeaders commonHeaders;
    private ApiConnection apiConnection;

    private Map<String, String> header;
    public static final String TAG = RestApiImpl.class.getSimpleName();

    /**
     * Constructor of the class
     *
     * @param context {@link Context}.
     */
    public RestApiImpl(Context context, EntityJsonMapper entityJsonMapper) {
        if (context == null) {
            throw new IllegalArgumentException("The constructor parameters cannot be null!!!");
        }
        this.context = context.getApplicationContext();
        this.gson = new Gson();
        this.entityJsonMapper = entityJsonMapper;

    }

    private String getMainURLDetails() throws MalformedURLException {
        String url = "";
        header.put(HEADER_AUTHORIZATION, "");
        apiConnection = ApiConnection.createGET(url, header);
        return apiConnection.requestSyncCall();

    }

    @Override
    public Observable<MainEntity> getMainEntityObservable() {
        return Observable.create(emitter -> {
            if (!isThereInternetConnection()) {
                emitter.onError(new Exception());
                return;
            }
            try {
                String configurationData = getMainURLDetails();

                if (configurationData != null) {
                    emitter.onNext(entityJsonMapper.transformMainEntity(configurationData));
                    emitter.onComplete();

                } else {
                    emitter.onError(new NetworkErrorException());
                }
            } catch (Exception e) {
                emitter.onError(new NetworkConnectionException(e.getCause()));
            }
        });
    }

    private boolean isThereInternetConnection() {
        boolean isConnected;

        ConnectivityManager connectivityManager =
                (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());

        return isConnected;
    }
}
