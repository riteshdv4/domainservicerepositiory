package com.domainservice.com.service.net;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lp-ritesh on 27/4/17.
 */


/**
 * Created by lp-ritesh on 25/4/17.
 */

public class CommonHeaders {


    private static final String sharedPreferenceKey = "com.android.decurtis.ibguestapp.IBSP";

    private static final String TOKEN_TYPE = "TokenType";
    private static final String AUTHORIZATION = "Authorization";
    private static final String DASHBOARD_NAME = "DashboardName";
    private static final String TIME_DIFFERENCE = "TimeDifference";
    private static final int DEFAULT_TIME_DIFFERENCE = 0;

    public static Map<String, String> getCommonHeaders(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(sharedPreferenceKey, Context.MODE_PRIVATE);
        String authorization = preferences.getString(TOKEN_TYPE, "") + " "
                + preferences.getString(AUTHORIZATION, "");

        String appMode = preferences.getString(DASHBOARD_NAME, "");

        String modifiedTime = String.valueOf(System.currentTimeMillis() +
                +preferences.getLong(TIME_DIFFERENCE, DEFAULT_TIME_DIFFERENCE));

        Map<String, String> headers = new HashMap<>();
//        headers.put(RestApi.STR_AUTHORIZATION, authorization);
//        headers.put(RestApi.STR_APP_MODE, appMode);
//        headers.put(RestApi.STR_MODIFIED_DATE, modifiedTime);
        return headers;

    }
}
